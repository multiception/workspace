FROM registry.gitlab.utc.fr/postprocessing/docker/workspace

# additional ROS packages
RUN pip install pyqtgraph sortedcontainers
WORKDIR /WS
